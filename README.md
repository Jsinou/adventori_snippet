# ADventori snippets

[ADventori snippets](https://marketplace.visualstudio.com/items?itemName=ADjsinou.adventorisnippet) is a tool that permits you to create your banner quickly.
All the snippets are prefixed by the key "ad" to find them easily.
We recommand you to use ADventori's css snippets if you use the HTML ones.

## NEW

### v0.3.13
- Edit font-face init (delete eot)

## Snippets

###### `<HTML>`
* **adbanner** : The main architecture of a banner with the dynamisation script. It contains all the HTML elements of the banner. The dynamic elements must be initialized and integrated.
* **adcarousel_init** : To initialize the carousel data into the `<script>
` tag. You have to type the different carousel elements and precise their values (exemple : productImg:'img/product.jpg').
###### `{css}`
* **adcssinit** : CSS base code for your ad container. Press tab to initialise height and width of the ad.
* **adfont** : Allows custom fonts to be loaded on your ad. You only have to type the name of your font. You have to place all your fonts files in a "fonts" directory at the root of your ad directory : **YourAdDirectory/fonts/YourFontFile**
* **adcenter** : Allows you to center an element in the horizontal axis.
###### `javascript`
* **adsetText** : To integrate a dynamic text. `ADventori.Display.setText(element, data)` replace element and data according to your element and its data.
* **adadaptText** : To adapt the size of your text in the container.`ADventori.Display.adaptText(element, minFontSize)` precise the element and the minimum font-size you want to apply depending of the text length.
* **adsetAndAdaptText** : To integrate and adapt a dynamic text at the same time. `ADventori.Display.setAndAdaptText(element, data, minFontSize)`.
* **adverticalAlign** : To align an element in the verical axis. `ADventori.Display.verticalAlign(element)` and precise the element you want to align.
* **adsetImageUrl** : To ad a dynamic image in your ad. `ADventori.Display.setImageUrl(element, urlImage, urlDefaultImage)` precise the element, the path of the image and the path of a default image.
* **adadaptImage** : To adapt the size of the image in the container. `ADventori.Display.adaptImage(element)`.
* **adaddAndAdaptImage** : `ADventori.Display.addAndAdaptImage(element, urlImage, urlDefaultImage, displayOption)`
* **adcarousel_init** : To initialize the product flows of the carousel.
* **adcarousel_congif** : To declare and configure the carousel.
* **adexist** : A function that allows you to verify if the element exists.
