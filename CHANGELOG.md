## Changelog

### v0.3.6
- Fixed README.md issue

### v0.3.5
- Add CHANGELOG.md
- Add README.md

### v0.3.4
- Fixed some issues

### v0.3.3
- Add prefix ad for all the functions

### v0.3.2
- Fixed some issues

### v0.3.1
- Add Icon

### v0.3.0 
- Add all JS snippets

### v0.2.0
- Add all CSS snippets

### v0.1.0 
- Add all HTML snippets

### v0.0.1 
- Initial 